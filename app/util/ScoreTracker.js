import {
  getFirestore,
  doc,
  setDoc,
  collection,
  query,
  where,
  getDoc,
  getDocs,
} from "firebase/firestore";

const db = getFirestore();

export const saveScore = async (uuid, username, score) => {
  if (score === 0) {
    return;
  }

  const scoreRef = doc(collection(db, "scores"), uuid);

  const docSnapshot = await getDoc(scoreRef);
  const existingData = docSnapshot.exists() ? docSnapshot.data() : null;

  let scores = [];
  let highScore = score;

  if (existingData) {
    if (existingData.scores) {
      scores = existingData.scores.split(",").map((s) => parseInt(s.trim()));
    }
    if (existingData.highScore) {
      highScore = existingData.highScore;
    }
  }

  if (score > highScore) {
    highScore = score;
  }

  scores.push(score);
  const scoresString = scores.join(", ");

  await setDoc(scoreRef, {
    username: username,
    scores: scoresString,
    highScore: highScore,
  });

  return highScore;
};

export const fetchScores = async (uuid) => {
  const scores = [];

  const q = query(collection(db, "scores"), where("uuid", "==", uuid));
  const querySnapshot = await getDocs(q);
  querySnapshot.forEach((doc) => {
    scores.push(doc.data());
  });

  return scores;
};

export async function getHighScore(userId) {
  const db = getFirestore();
  const userDocRef = doc(db, "scores", userId);
  const userDocSnapshot = await getDoc(userDocRef);

  if (userDocSnapshot.exists()) {
    const userData = userDocSnapshot.data();
    return userData.highScore || 0;
  } else {
    return 0;
  }
}

export async function fetchUserHighScore(userId, setHighScoreFunc) {
  const userHighScore = await getHighScore(userId);
  setHighScoreFunc(userHighScore);
}

export const fetchAllHighScores = async () => {
  const scores = [];

  const scoresCollection = collection(db, "scores");
  const querySnapshot = await getDocs(scoresCollection);

  querySnapshot.forEach((doc) => {
    scores.push(doc.data());
  });

  if (scores.length > 0) {
    let uniqueHighScores = scores.map((data) => ({
      username: data.username,
      highScore: data.highScore,
    }));

    // Sort the high scores in descending order
    uniqueHighScores.sort((a, b) => b.highScore - a.highScore);

    return uniqueHighScores;
  } else {
    return null;
  }
};
