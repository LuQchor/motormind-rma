import { collection, getDocs } from "firebase/firestore";
import { db } from "../Firebase/firebase";
import { Alert } from "react-native";

const fetchQuestionData = async (
  correctCount,
  prevQuestions,
  setQuestionData,
  setLoadNewQuestion
) => {
  const collectionName =
    correctCount < 15
      ? correctCount % 3 === 0 && correctCount !== 0
        ? "englishQuestionsHard"
        : "englishQuestionsEasy"
      : correctCount % 3 === 0
      ? "englishQuestionsEasy"
      : "englishQuestionsHard";
  const querySnapshot = await getDocs(collection(db, collectionName));

  const questions = querySnapshot.docs.map((doc) => ({
    id: doc.id,
    ...doc.data(),
  }));

  const newQuestions = questions.filter(
    (question) => !prevQuestions.includes(question.id)
  );

  if (newQuestions.length === 0) {
    Alert.alert("You have answered all questions from this collection!");
    return;
  }

  const randomIndex = Math.floor(Math.random() * newQuestions.length);
  const randomQuestion = newQuestions[randomIndex];

  const options = randomQuestion.options
    ? randomQuestion.options.split(",").map((option) => option.trim())
    : [];

  const randomizedOptions = options.sort(() => Math.random() - 0.5);

  const updatedQuestionData = {
    ...randomQuestion,
    options: randomizedOptions,
  };

  setQuestionData(updatedQuestionData);
  setLoadNewQuestion(false);
};

export default fetchQuestionData;
