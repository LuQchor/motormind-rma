import AsyncStorage from "@react-native-async-storage/async-storage";

export const saveScoreGuest = async (score) => {
  try {
    const highScore = await getHighScoreGuest();
    if (score > highScore) {
      await AsyncStorage.setItem("@guestHighScore", JSON.stringify(score));
      return score;
    } else {
      return highScore;
    }
  } catch (error) {
    console.error(error);
  }
};

export const getHighScoreGuest = async () => {
  try {
    const highScore = await AsyncStorage.getItem("@guestHighScore");
    if (highScore !== null) {
      return JSON.parse(highScore);
    }
    return 0;
  } catch (error) {
    console.error(error);
    return 0;
  }
};

export async function fetchGuestHighScore(setHighScoreFunc) {
  const guestHighScore = await getHighScoreGuest();
  setHighScoreFunc(guestHighScore);
}
