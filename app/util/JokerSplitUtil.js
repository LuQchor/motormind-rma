export default function JokerSplitSelected(
  questionData,
  setQuestionData,
  jokerSplitUsed,
  setJokerSplitUsed
) {
  if (jokerSplitUsed) {
    return;
  }

  if (!questionData) {
    return;
  }

  const { options, answer } = questionData;

  // Find the indices of the wrong options
  const wrongOptionIndices = options.reduce((acc, option, index) => {
    if (option.toLowerCase() !== answer.toLowerCase()) {
      acc.push(index);
    }
    return acc;
  }, []);

  if (wrongOptionIndices.length >= 2) {
    const shuffledIndices = wrongOptionIndices.sort(() => Math.random() - 0.5);

    // Remove the first two wrong options
    const updatedOptions = [...options];
    updatedOptions[shuffledIndices[0]] = "";
    updatedOptions[shuffledIndices[1]] = "";

    // Set the updated question data
    const updatedQuestionData = {
      ...questionData,
      options: updatedOptions,
    };
    setQuestionData(updatedQuestionData);
  }

  setJokerSplitUsed(true);
}
