import React from "react";
import { StyleSheet, View, Modal, Dimensions } from "react-native";
import { BarChart } from "react-native-chart-kit";
import AppButton from "../components/AppButton";

const windowWidth = Dimensions.get("window").width;
const windowHeight = Dimensions.get("window").height;

export default function JokerGroupUtil({ isVisible, onClose, questionData }) {
  if (!questionData) return null;

  const getRandomPercent = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  };

  const correctAnswer = questionData?.answer;
  const correctOptionIndex = questionData.options.findIndex(
    (opt) => opt.toLowerCase() === correctAnswer.toLowerCase()
  );
  const percentages = new Array(4).fill(0);
  let remaining = 100 - getRandomPercent(62, 96);

  percentages[correctOptionIndex] = 100 - remaining;

  let rest = [0, 1, 2, 3].filter((val) => val !== correctOptionIndex);

  rest.forEach((value, index) => {
    if (index !== rest.length - 1) {
      let thisVal = getRandomPercent(0, remaining);
      percentages[value] = thisVal;
      remaining -= thisVal;
    } else {
      percentages[value] = remaining;
    }
  });

  const data = {
    labels: questionData.options.map((option) =>
      option.length > 10 ? `${option.substring(0, 10)}...` : option
    ),
    datasets: [
      {
        data: percentages,
      },
    ],
  };

  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={isVisible}
      onRequestClose={onClose}
    >
      <View style={styles.centeredView}>
        <View style={[styles.modalView, { width: windowWidth * 0.9 }]}>
          <BarChart
            data={data}
            width={windowWidth * 0.9}
            height={windowHeight * 0.4}
            fromZero={true}
            chartConfig={{
              backgroundGradientFrom: "#fff",
              backgroundGradientTo: "#fff",
              decimalPlaces: 0,
              color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
              labelColor: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
            }}
            style={{
              borderRadius: 16,
            }}
          />
          <AppButton
            title="Back"
            onPress={onClose}
            color="buttonmaincolor"
            style={styles.closeButton}
          />
        </View>
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: windowHeight * 0.02,
  },
  modalView: {
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  closeButton: {
    flexDirection: "column",
    marginTop: windowHeight * 0.02,
  },
});
