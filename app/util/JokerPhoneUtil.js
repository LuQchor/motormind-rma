import React from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Modal,
  StyleSheet,
  Dimensions,
} from "react-native";
import colors from "../config/colors";

const windowWidth = Dimensions.get("window").width;
const windowHeight = Dimensions.get("window").height;

const JokerPhoneUtil = ({ isVisible, onOptionSelected, onClose }) => {
  const people = ["Elon Musk", "Donut Media", "ChrisFix"];

  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={isVisible}
      onRequestClose={onClose}
    >
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          <Text style={styles.modalText}>Who do you want to call? {"\n"}</Text>

          {people.map((person, index) => (
            <TouchableOpacity
              key={index}
              onPress={() => onOptionSelected(person)}
              style={styles.optionButton}
            >
              <Text style={styles.optionText}>{person} </Text>
            </TouchableOpacity>
          ))}
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: windowHeight * 0.05,
  },
  modalView: {
    marginHorizontal: windowWidth * 0.1,
    backgroundColor: "white",
    borderRadius: windowWidth * 0.05,
    paddingVertical: windowHeight * 0.03,
    paddingHorizontal: windowWidth * 0.05,
    alignItems: "center",
    shadowColor: colors.black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  modalText: {
    marginBottom: windowHeight * 0.02,
    fontSize: windowWidth * 0.04,
    textAlign: "center",
    color: colors.black,
  },
  optionButton: {
    marginBottom: windowHeight * 0.015,
    paddingVertical: windowHeight * 0.01,
    paddingHorizontal: windowWidth * 0.05,
    borderRadius: windowWidth * 0.025,
    backgroundColor: colors.lightGray,
  },
  optionText: {
    fontSize: windowWidth * 0.04,
    textAlign: "center",
    color: colors.black,
  },
});

export default JokerPhoneUtil;
