import { Audio } from "expo-av";

async function playSound(file) {
  const { sound } = await Audio.Sound.createAsync(file);

  await sound.setOnPlaybackStatusUpdate(async (playbackStatus) => {
    if (playbackStatus.didJustFinish) {
      await sound.unloadAsync();
    }
  });

  await sound.playAsync();
}

export async function playCorrectSound() {
  try {
    await playSound(require("../assets/audio/Correct.mp3"));
  } catch (error) {}
}

export async function playIncorrectSound() {
  try {
    await playSound(require("../assets/audio/Incorrect.mp3"));
  } catch (error) {}
}

export async function playCongratulationsSound() {
  try {
    await playSound(require("../assets/audio/Congratulations.mp3"));
  } catch (error) {}
}

export async function playJokerSound() {
  try {
    await playSound(require("../assets/audio/Joker.mp3"));
  } catch (error) {}
}
