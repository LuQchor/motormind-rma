export default {
  backgroundcolor: "#D9F4F8",
  buttonmaincolor: "#8FE6F2",
  textareacolor: "#1E96B6",
  redcolor: "#ff0000",
  greencolor: "#008000",
  white: "#fff",
  black: "#000",
};
