import { initializeApp, getApps } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyCecyMeK0LqrGa8pD066eoo1OEZcZ3zqDQ",
  authDomain: "motormind-rma.firebaseapp.com",
  databaseURL:
    "https://motormind-rma-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "motormind-rma",
  storageBucket: "motormind-rma.appspot.com",
  messagingSenderId: "170871966081",
  appId: "1:170871966081:web:a684610c9c8a5f430b4080",
  measurementId: "G-J5G26SNPGK",
};

let app;

if (getApps().length === 0) {
  app = initializeApp(firebaseConfig);
} else {
  app = getApps()[0];
}

const auth = getAuth(app);
const db = getFirestore(app);
export { auth, db };
