import React, { useEffect, useState } from "react";
import {
  SafeAreaView,
  StyleSheet,
  Text,
  Image,
  View,
  Dimensions,
} from "react-native";
import { AntDesign, FontAwesome5 } from "@expo/vector-icons";

import AppButton from "../components/AppButton";
import colors from "../config/colors";

import { auth } from "../Firebase/firebase";

const { width, height } = Dimensions.get("window");

function HomeScreen({ navigation }) {
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged((user) => {
      setIsLoggedIn(user !== null);
    });
    return unsubscribe;
  }, []);

  return (
    <View style={styles.viewcolour}>
      <SafeAreaView style={styles.container}>
        <Text style={styles.title}>MotorMind</Text>
        <Image
          style={[
            styles.image,
            { width: width * 0.7, marginBottom: height * 0.03 },
          ]}
          source={require("../assets/image/background.png")}
        />
        {isLoggedIn ? (
          <>
            <AppButton
              title="Play"
              onPress={() => navigation.navigate("Play", { isGuest: false })}
              style={styles.button}
              IconComponent={AntDesign}
              iconName="play"
              iconColor="black"
            />
            <AppButton
              title="Profile"
              onPress={() => navigation.navigate("Profile")}
              style={styles.button}
              IconComponent={FontAwesome5}
              iconName="user-circle"
              iconColor="black"
            />
          </>
        ) : (
          <>
            <AppButton
              title="Login"
              onPress={() => navigation.navigate("Login")}
              style={styles.button}
              IconComponent={AntDesign}
              iconName="user"
              iconColor="black"
            />
            <AppButton
              title="Play as guest"
              onPress={() => navigation.navigate("Play", { isGuest: true })}
              style={styles.button}
              IconComponent={AntDesign}
              iconName="user"
              iconColor="gray"
            />
          </>
        )}
        <AppButton
          title="High score"
          onPress={() => navigation.navigate("HighScore")}
          style={styles.button}
          IconComponent={FontAwesome5}
          iconName="trophy"
          iconColor="gold"
        />
      </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    paddingTop: height * 0.07,
    paddingBottom: height * 0.03,
    justifyContent: "center",
  },
  image: {
    marginTop: height * 0.05,
    height: "50%",
    alignSelf: "center",
  },
  title: {
    fontSize: height * 0.07,
    paddingBottom: height * 0.01,
  },
  viewcolour: {
    flex: 1,
    backgroundColor: colors.backgroundcolor,
  },
  button: {
    marginTop: height * 0.01,
  },
});

export default HomeScreen;
