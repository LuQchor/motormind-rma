import { createStackNavigator } from "@react-navigation/stack";
import { Platform } from "react-native";

import HomeScreen from "./HomeScreen";
import LoginScreen from "./LoginScreen";
import RegisterScreen from "./RegisterScreen";
import ProfileScreen from "./ProfileScreen";
import PlayScreen from "./PlayScreen";
import HighScoreScreen from "./HighScoreScreen";

const Stack = createStackNavigator();

function AppNavigator() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={HomeScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Login"
        component={LoginScreen}
        options={{
          headerShown: shouldShowHeader(),
          headerTitle: "Login",
        }}
      />
      <Stack.Screen
        name="Register"
        component={RegisterScreen}
        options={{
          headerShown: shouldShowHeader(),
          headerTitle: "Register",
        }}
      />
      <Stack.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          headerShown: shouldShowHeader(),
          headerTitle: "Profile",
        }}
      />
      <Stack.Screen
        name="Play"
        component={PlayScreen}
        options={{
          headerShown: shouldShowHeader(),
          headerTitle: "Play",
        }}
      />
      <Stack.Screen
        name="HighScore"
        component={HighScoreScreen}
        options={{
          headerShown: shouldShowHeader(),
          headerTitle: "HighScore",
        }}
      />
    </Stack.Navigator>
  );
}

function shouldShowHeader() {
  return Platform.OS === "ios" ? true : false;
}

export default AppNavigator;
