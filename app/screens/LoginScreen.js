import React, { useState, useRef } from "react";
import {
  StyleSheet,
  View,
  SafeAreaView,
  Dimensions,
  Alert,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import { AntDesign } from "@expo/vector-icons";

import AppTextInput from "../components/AppTextInput";
import AppButton from "../components/AppButton";
import colors from "../config/colors";

import { signInWithEmailAndPassword } from "firebase/auth";
import { auth } from "../Firebase/firebase";

const { width, height } = Dimensions.get("window");

function LoginScreen(props) {
  const navigation = useNavigation();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const passwordInputRef = useRef(null);

  const handleLogin = () => {
    signInWithEmailAndPassword(auth, email, password)
      .then(() => {
        navigation.popToTop();
        Alert.alert("Successful login");
      })
      .catch((error) => {
        if (
          error.code === "auth/user-not-found" ||
          error.code === "auth/wrong-password"
        ) {
          Alert.alert("Invalid Email or password");
        } else {
          Alert.alert("Please enter Email and password");
          console.error(error);
        }
      });
  };

  const handleEmailSubmit = () => {
    passwordInputRef.current.focus();
  };

  const handlePasswordSubmit = () => {
    handleLogin();
  };

  return (
    <View style={styles.viewcolour}>
      <SafeAreaView style={styles.container}>
        <KeyboardAvoidingView
          behavior={Platform.OS === "ios" ? "padding" : "height"}
          style={styles.keyboardAvoidingContainer}
        >
          <View style={styles.scrollContainer}>
            <View style={styles.addButtonContainer}>
              <AppButton
                onPress={() => navigation.navigate("Register")}
                style={styles.addButton}
                IconComponent={AntDesign}
                iconName="addusergroup"
                iconColor="black"
              />
            </View>
            <ScrollView contentContainerStyle={styles.contentContainer}>
              <AppTextInput
                placeholder="Email"
                onChangeText={(text) => setEmail(text)}
                IconComponent={AntDesign}
                iconName="mail"
                iconColor="black"
                onSubmitEditing={handleEmailSubmit}
              />
              <AppTextInput
                ref={passwordInputRef}
                placeholder="Password"
                secureTextEntry
                onChangeText={(text) => setPassword(text)}
                value={password}
                IconComponent={AntDesign}
                iconName="lock"
                iconColor="black"
                onSubmitEditing={handlePasswordSubmit}
              />
            </ScrollView>
            <View style={styles.loginButtonContainer}>
              <AppButton
                title="Login"
                style={styles.loginButton}
                onPress={handleLogin}
                IconComponent={AntDesign}
                iconName="login"
                iconColor="black"
              />
            </View>
          </View>
        </KeyboardAvoidingView>
      </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  viewcolour: {
    flex: 1,
    backgroundColor: colors.backgroundcolor,
  },
  keyboardAvoidingContainer: {
    flex: 1,
  },
  scrollContainer: {
    flex: 1,
  },
  contentContainer: {
    flexGrow: 1,
    justifyContent: "center",
    alignItems: "center",
    paddingTop: height * 0.07,
    paddingBottom: height * 0.05,
  },
  addButtonContainer: {
    alignSelf: "flex-end",
    marginTop: height * 0.045,
    marginRight: width * 0.05,
  },
  addButton: {
    width: width * 0.15,
    height: width * 0.15,
    borderRadius: (width * 0.15) / 2,
  },
  loginButtonContainer: {
    marginBottom: height * 0.03,
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  loginButton: {
    width: "70%",
  },
});

export default LoginScreen;
