import React, { useEffect, useState } from "react";

import {
  StyleSheet,
  View,
  SafeAreaView,
  Dimensions,
  Text,
  Alert,
} from "react-native";

import colors from "../config/colors";
import AppButton from "../components/AppButton";
import { auth } from "../Firebase/firebase";

import { Entypo, MaterialCommunityIcons } from "@expo/vector-icons";

import { saveScore, fetchUserHighScore } from "../util/ScoreTracker";
import { saveScoreGuest, fetchGuestHighScore } from "../util/ScoreTrackerGuest";
import fetchQuestionData from "../util/QuestionsUtil";

import JokerPhoneUtil from "../util/JokerPhoneUtil";
import JokerSplitSelected from "../util/JokerSplitUtil";
import JokerGroupUtil from "../util/JokerGroupUtil";
const {
  playCorrectSound,
  playIncorrectSound,
  playCongratulationsSound,
  playJokerSound,
} = require("../util/Sounds");

const { width, height } = Dimensions.get("window");

export default function PlayScreen({ route }) {
  const [questionData, setQuestionData] = useState(null);
  const [buttonColor, setButtonColor] = useState(null);
  const [selectedOptionIndex, setSelectedOptionIndex] = useState(null);
  const [correctCount, setCorrectCount] = useState(0);
  const [prevQuestions, setPrevQuestions] = useState([]);
  const [loadNewQuestion, setLoadNewQuestion] = useState(true);
  const [disableAnswerButton, setDisableAnswerButton] = useState(false);

  const { isGuest } = route.params;
  const [highScore, setHighScore] = useState(0);
  const [isPhonePopupVisible, setIsPhonePopupVisible] = useState(false);
  const [jokerSplitUsed, setJokerSplitUsed] = useState(false);
  const [phoneButtonUsed, setPhoneButtonUsed] = useState(false);

  const [isFetchingQuestion, setIsFetchingQuestion] = useState(false); // fixed the bug that would apply Jokersplit to previously loaded questions
  const [isGroupPopupVisible, setIsGroupPopupVisible] = useState(false);
  const [groupButtonUsed, setGroupButtonUsed] = useState(false);

  const JokerPhoneSelectedWrapper = (selectedPerson) => {
    setIsPhonePopupVisible(false);

    const correctAnswer = questionData?.answer;

    if (correctAnswer) {
      Alert.alert(
        `${selectedPerson} says:`,
        `The correct answer is ${correctAnswer}`
      );
    }
  };

  const JokerSplitSelectedWrapper = () => {
    if (!disableAnswerButton && !isFetchingQuestion) {
      playJokerSound();
      JokerSplitSelected(
        questionData,
        setQuestionData,
        jokerSplitUsed,
        setJokerSplitUsed
      );
    }
  };

  let newHighScore = 0;

  const checkAnswer = async (selectedAnswer, index) => {
    if (disableAnswerButton) {
      return;
    }

    if (selectedAnswer.toLowerCase() === questionData.answer.toLowerCase()) {
      setSelectedOptionIndex(index);
      setButtonColor("green");
      setCorrectCount(correctCount + 1);
      setPrevQuestions((prev) => [...prev, questionData.id]);
      playCorrectSound();
    } else {
      if (correctCount > highScore) {
        Alert.alert("Congratulations!", "You set a new high score!");
        playCongratulationsSound();
      } else {
        playIncorrectSound();
      }
      setSelectedOptionIndex(index);
      setButtonColor("red");
      if (isGuest) {
        newHighScore = await saveScoreGuest(correctCount);
      } else {
        newHighScore = await saveScore(
          auth.currentUser.uid,
          auth.currentUser.displayName,
          correctCount
        );
      }

      // Reset the score and start a new game

      setCorrectCount(0);
      setPhoneButtonUsed(false);
      setJokerSplitUsed(false);
      setGroupButtonUsed(false);
      setPrevQuestions([]);
    }

    setDisableAnswerButton(true);

    setTimeout(() => {
      setSelectedOptionIndex(null);
      setButtonColor(null);
      setLoadNewQuestion(true);
      setDisableAnswerButton(false);
    }, 800);
  };

  useEffect(() => {
    const fetchQuestion = async () => {
      if (loadNewQuestion) {
        setIsFetchingQuestion(true);
        setDisableAnswerButton(true); // Disable the button when you start fetching
        await fetchQuestionData(
          correctCount,
          prevQuestions,
          setQuestionData,
          setLoadNewQuestion
        );
        setIsFetchingQuestion(false);
        setDisableAnswerButton(false); // Enable the button when the new question is fetched and set
      }
    };

    if (isGuest) {
      fetchGuestHighScore(setHighScore);
    } else {
      fetchUserHighScore(auth.currentUser.uid, setHighScore);
    }

    fetchQuestion();
  }, [loadNewQuestion, correctCount, isGuest]);

  return (
    <View style={styles.container}>
      <SafeAreaView>
        <View style={styles.counter}>
          <Text style={styles.counterText}>Highscore: {highScore}</Text>
          <Text style={styles.counterText}>
            Correct answers: {correctCount}
          </Text>
        </View>
        <View style={styles.question}>
          {questionData && (
            <Text style={styles.questionText}>{questionData.question}</Text>
          )}
        </View>
        <View style={styles.buttonContainer}>
          {questionData && (
            <>
              {[
                questionData.options.slice(0, 2),
                questionData.options.slice(2, 4),
              ].map((buttonRow, rowIndex) => (
                <View key={rowIndex} style={styles.buttonRow}>
                  {buttonRow.map((option, index) => (
                    <AppButton
                      textTransform="none"
                      key={index + rowIndex * 2}
                      title={option}
                      onPress={() => checkAnswer(option, index + rowIndex * 2)}
                      style={[
                        styles.button,
                        selectedOptionIndex === index + rowIndex * 2 &&
                          buttonColor === "green" && {
                            backgroundColor: colors.greencolor,
                          },
                        selectedOptionIndex === index + rowIndex * 2 &&
                          buttonColor === "red" && {
                            backgroundColor: colors.redcolor,
                          },
                      ]}
                      disabled={disableAnswerButton}
                    />
                  ))}
                </View>
              ))}
            </>
          )}
        </View>
        <View style={styles.jokerButtonContainer}>
          <AppButton
            onPress={() => {
              if (!phoneButtonUsed && !disableAnswerButton) {
                playJokerSound();
                setIsPhonePopupVisible(true);
                setPhoneButtonUsed(true);
              }
            }}
            style={[
              styles.buttonJoker,
              phoneButtonUsed && { backgroundColor: colors.redcolor },
            ]}
            IconComponent={Entypo}
            iconName="phone"
            iconColor="black"
            disabled={disableAnswerButton || phoneButtonUsed}
          />
          <AppButton
            onPress={JokerSplitSelectedWrapper}
            style={[
              styles.buttonJoker,
              jokerSplitUsed && { backgroundColor: colors.redcolor },
            ]}
            IconComponent={MaterialCommunityIcons}
            iconName="fraction-one-half"
            iconColor="black"
            disabled={disableAnswerButton || jokerSplitUsed}
          />
          <AppButton
            onPress={() => {
              if (!groupButtonUsed && !disableAnswerButton) {
                playJokerSound();
                setIsGroupPopupVisible(true);
                setGroupButtonUsed(true);
              }
            }}
            style={[
              styles.buttonJoker,
              groupButtonUsed && { backgroundColor: colors.redcolor },
            ]}
            IconComponent={MaterialCommunityIcons}
            iconName="account-group"
            iconColor="black"
            disabled={disableAnswerButton || groupButtonUsed}
          />
        </View>
        <JokerGroupUtil
          isVisible={isGroupPopupVisible}
          onClose={() => setIsGroupPopupVisible(false)}
          questionData={questionData}
        />
        <JokerPhoneUtil
          isVisible={isPhonePopupVisible}
          onOptionSelected={JokerPhoneSelectedWrapper}
          onClose={() => setIsPhonePopupVisible(false)}
        />
      </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create({
  button: {
    flex: 1,
    margin: height * 0.003,
  },
  buttonRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: height * 0.01,
  },
  buttonContainer: {
    marginBottom: height * 0.045,
  },
  buttonJoker: {
    marginHorizontal: width * 0.05,
    marginBottom: height * 0.02,
    width: width * 0.15,
    height: width * 0.15,
    borderRadius: (width * 0.15) / 2,
  },
  counter: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: height * 0.025,
  },
  counterText: {
    fontSize: height * 0.026,
    color: colors.textareacolor,
  },
  jokerButtonContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: height * 0.03,
  },
  container: {
    flex: 1,
    backgroundColor: colors.backgroundcolor,
    alignItems: "center",
    justifyContent: "flex-end",
  },
  question: {
    width: "80%",
    backgroundColor: colors.textareacolor,
    marginBottom: height * 0.035,
    marginLeft: width * 0.05,
    marginRight: width * 0.05,
  },
  questionText: {
    color: colors.white,
    fontSize: height * 0.025,
    textAlign: "center",
    padding: 10,
  },
});
