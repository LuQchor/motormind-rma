import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  Alert,
  SafeAreaView,
  ScrollView,
  Dimensions,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import { AntDesign } from "@expo/vector-icons";
import AppButton from "../components/AppButton";
import colors from "../config/colors";
import { auth } from "../Firebase/firebase";
import { getFirestore, collection, doc, getDoc } from "firebase/firestore";
const { width, height } = Dimensions.get("window");

function ProfileScreen() {
  const navigation = useNavigation();
  const [username, setUsername] = useState("");
  const [scores, setScores] = useState([]);
  const [highScore, setHighScore] = useState(0);

  useEffect(() => {
    const isLogged = auth.onAuthStateChanged((user) => {
      if (user) {
        setUsername(user.displayName);
        fetchUserScores(user.uid);
      } else {
        setUsername("");
        setScores([]);
        setHighScore(0);
      }
    });

    return isLogged;
  }, []);

  const fetchUserScores = async (uuid) => {
    try {
      const db = getFirestore();
      const scoreRef = doc(collection(db, "scores"), uuid);
      const docSnapshot = await getDoc(scoreRef);
      const data = docSnapshot.exists() ? docSnapshot.data() : null;

      if (data) {
        const scoresString = data.scores || "";
        const scoresArray = scoresString
          .split(",")
          .map((s) => parseInt(s.trim()));
        setScores(scoresArray);
        setHighScore(data.highScore || 0);
      } else {
        setScores([]);
        setHighScore(0);
      }
    } catch (error) {
      console.error("Error fetching scores:", error);
    }
  };

  const handleLogout = () => {
    auth
      .signOut()
      .then(() => {
        navigation.popToTop();
        Alert.alert("Successful logout");
      })
      .catch((error) => {
        Alert.alert("Error", error.message);
      });
  };

  return (
    <View style={styles.container}>
      <SafeAreaView>
        <Text style={styles.title}>Hello, {username}</Text>
        <View style={[styles.scoresContainer, { maxHeight: height * 0.5 }]}>
          <Text style={styles.scoreText}>Highest Score: {highScore}</Text>
          <ScrollView>
            {scores.length > 0 ? (
              scores.map((score, index) => (
                <Text key={index} style={styles.scoreText}>
                  Score {index + 1}: {score}
                </Text>
              ))
            ) : (
              <Text style={styles.noScoresText}>No scores to display.</Text>
            )}
          </ScrollView>
        </View>
        <View style={styles.logoutContainer}>
          <AppButton
            title="Logout"
            onPress={handleLogout}
            style={styles.logoutButton}
            IconComponent={AntDesign}
            iconName="logout"
            iconColor="black"
          />
        </View>
      </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.backgroundcolor,
    alignItems: "center",
  },
  title: {
    fontSize: height * 0.045,
    fontWeight: "bold",
    color: colors.textareacolor,
    marginTop: height * 0.035,
    marginBottom: height * 0.01,
    textAlign: "center",
  },
  scoresContainer: {
    flexGrow: 1,
    alignItems: "center",
    paddingBottom: height * 0.03,
  },
  scoreText: {
    fontSize: height * 0.024,
    color: colors.textareacolor,
    marginBottom: height * 0.012,
  },
  noScoresText: {
    fontSize: height * 0.028,
    color: colors.textareacolor,
    textAlign: "center",
  },
  logoutContainer: {
    alignItems: "center",
    justifyContent: "flex-end",
    flex: 1,
    marginBottom: height * 0.03,
  },
  logoutButton: {
    width: width * 0.7,
  },
});

export default ProfileScreen;
