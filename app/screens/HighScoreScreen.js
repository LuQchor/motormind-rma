import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  ScrollView,
  Dimensions,
} from "react-native";
import { fetchAllHighScores } from "../util/ScoreTracker";
import colors from "../config/colors";

const { height } = Dimensions.get("window");

function HighScoreScreen() {
  const [highScores, setHighScores] = useState([]);
  const screenHeight = Dimensions.get("window").height;

  useEffect(() => {
    fetchHighScores();
  }, []);

  const fetchHighScores = async () => {
    try {
      const scoresData = await fetchAllHighScores();

      if (scoresData) {
        setHighScores(scoresData);
      } else {
        console.log("No scores to display");
      }
    } catch (error) {
      console.error("Error fetching high scores:", error);
    }
  };

  return (
    <View style={styles.container}>
      <SafeAreaView>
        <Text style={styles.title}>High Scores</Text>
        <ScrollView
          style={{ maxHeight: screenHeight * 0.5 }}
          contentContainerStyle={styles.scoresContainer}
        >
          {highScores.length > 0 ? (
            highScores.map((scoreData, index) => (
              <Text key={index} style={styles.scoreText}>
                {scoreData.username}: {scoreData.highScore}
              </Text>
            ))
          ) : (
            <Text style={styles.noScoresText}>No high scores to display.</Text>
          )}
        </ScrollView>
      </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.backgroundcolor,
    alignItems: "center",
  },
  title: {
    fontSize: height * 0.045,
    fontWeight: "bold",
    color: colors.textareacolor,
    marginTop: height * 0.035,
    marginBottom: height * 0.01,
    textAlign: "center",
  },
  scoresContainer: {
    flexGrow: 1,
    alignItems: "center",
    paddingBottom: height * 0.03,
  },
  scoreText: {
    fontSize: height * 0.024,
    color: colors.textareacolor,
    marginBottom: height * 0.012,
  },
  noScoresText: {
    fontSize: height * 0.028,
    color: colors.textareacolor,
    textAlign: "center",
  },
});

export default HighScoreScreen;
