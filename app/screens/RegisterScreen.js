import React, { useState, useRef } from "react";
import {
  StyleSheet,
  View,
  SafeAreaView,
  Dimensions,
  Alert,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import { AntDesign } from "@expo/vector-icons";

import AppTextInput from "../components/AppTextInput";
import AppButton from "../components/AppButton";
import colors from "../config/colors";

import {
  getAuth,
  createUserWithEmailAndPassword,
  updateProfile,
} from "firebase/auth";

const { width, height } = Dimensions.get("window");

function RegisterScreen(props) {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const passwordInputRef = useRef(null);
  const emailInputRef = useRef(null);

  const navigation = useNavigation();

  const auth = getAuth();

  const handleRegister = () => {
    createUserWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        updateProfile(userCredential.user, {
          displayName: username,
        })
          .then(() => {
            navigation.popToTop();
            Alert.alert("Successful registration");
          })
          .catch((error) => {
            console.error(error);
          });
      })
      .catch((error) => {
        if (error.code === "auth/weak-password") {
          Alert.alert("The password is too weak.");
        } else if (error.code === "auth/email-already-in-use") {
          Alert.alert("The email address is already in use.");
        } else {
          console.error(error);
        }
      });
  };

  const handleUsernameSubmit = () => {
    passwordInputRef.current.focus();
  };
  const handlePasswordSubmit = () => {
    emailInputRef.current.focus();
  };
  const handleEmailSubmit = () => {
    handleRegister();
  };

  return (
    <View style={styles.viewcolour}>
      <SafeAreaView style={styles.container}>
        <KeyboardAvoidingView
          behavior={Platform.OS === "ios" ? "padding" : "height"}
          style={styles.keyboardAvoidingContainer}
        >
          <ScrollView contentContainerStyle={styles.scrollContainer}>
            <View style={styles.dontMove}></View>
            <AppTextInput
              placeholder="Username"
              onChangeText={(text) => setUsername(text)}
              IconComponent={AntDesign}
              iconName="user"
              iconColor="black"
              onSubmitEditing={handleUsernameSubmit}
            />
            <AppTextInput
              ref={passwordInputRef}
              placeholder="Password"
              secureTextEntry
              onChangeText={(text) => setPassword(text)}
              value={password}
              IconComponent={AntDesign}
              iconName="lock"
              iconColor="black"
              onSubmitEditing={handlePasswordSubmit}
            />
            <AppTextInput
              ref={emailInputRef}
              placeholder="Email"
              onChangeText={(text) => setEmail(text)}
              IconComponent={AntDesign}
              iconName="mail"
              iconColor="black"
              onSubmitEditing={handleEmailSubmit}
            />
          </ScrollView>
        </KeyboardAvoidingView>
        <AppButton
          title="Register"
          style={styles.registerButton}
          onPress={handleRegister}
          IconComponent={AntDesign}
          iconName="addusergroup"
          iconColor="black"
        />
      </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.backgroundcolor,
  },
  keyboardAvoidingContainer: {
    flex: 1,
  },
  scrollContainer: {
    flexGrow: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  registerButton: {
    alignSelf: "center",
    marginBottom: height * 0.03,
    width: "70%",
  },
  viewcolour: {
    flex: 1,
    backgroundColor: colors.backgroundcolor,
  },
});

export default RegisterScreen;
