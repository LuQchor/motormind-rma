import React from "react";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  Dimensions,
} from "react-native";
import colors from "../config/colors";

const { width, height } = Dimensions.get("window");

function AppButton({
  title,
  onPress,
  color = "buttonmaincolor",
  style,
  image,
  IconComponent,
  iconName,
  iconSize = 20,
  iconColor = "white",
  textColor = "#ffff",
  fontSize = 18,
  textTransform = "uppercase",
  fontWeight = "bold",
}) {
  return (
    <TouchableOpacity
      style={[styles.button, { backgroundColor: colors[color] }, style]}
      onPress={onPress}
    >
      {image ? (
        <Image source={image} style={styles.image} />
      ) : IconComponent ? (
        <IconComponent
          name={iconName}
          size={iconSize}
          color={iconColor}
          style={[styles.icon, title && { paddingRight: 10 }]}
        />
      ) : null}
      {title ? (
        <Text
          style={[
            styles.text,
            {
              color: textColor,
              fontSize: fontSize,
              textTransform: textTransform,
              fontWeight: fontWeight,
            },
          ]}
        >
          {title}
        </Text>
      ) : null}
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  button: {
    borderRadius: 25,
    justifyContent: "center",
    alignItems: "center",
    padding: width * 0.035,
    width: "70%",
    marginVertical: height * 0.01,
    flexDirection: "row",
  },
  text: {},
  icon: {},
  image: {
    height: 20,
    width: 20,
  },
});

export default AppButton;
