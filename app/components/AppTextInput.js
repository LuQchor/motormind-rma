import React, { forwardRef } from "react";
import { StyleSheet, TextInput, View } from "react-native";

import colors from "../config/colors";

const AppTextInput = forwardRef(
  (
    {
      placeholder,
      color = "white",
      IconComponent,
      style,
      iconName,
      iconSize = 20,
      iconColor = "white",
      ...otherProps
    },
    ref
  ) => {
    return (
      <View
        style={[styles.container, { backgroundColor: colors[color] }, style]}
      >
        {iconName && IconComponent && (
          <View style={styles.iconContainer}>
            <IconComponent
              name={iconName}
              size={iconSize}
              color={iconColor}
              style={styles.icon}
            />
          </View>
        )}
        <TextInput
          placeholder={placeholder}
          style={styles.textInput}
          {...otherProps}
          ref={ref}
        />
      </View>
    );
  }
);

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors,
    borderRadius: 25,
    flexDirection: "row",
    alignItems: "center",
    padding: 15,
    marginVertical: 10,
  },
  iconContainer: {
    marginRight: 10,
  },
  icon: {
    width: 20,
    height: 20,
  },
  textInput: {
    flex: 0.7,
    fontSize: 18,
    textTransform: "uppercase",
    marginRight: 10,
  },
});

export default AppTextInput;
